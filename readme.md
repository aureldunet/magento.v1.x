# Docker Magento v1.x

![Magento](m1.png)

v.1.9.4.2

## Requirements

- git
- docker
- docker-compose
- make

## Install

Run

```
git config core.fileMode false
```

Run 

```
make install
```

Import database

```
docker-compose exec mysql bash

mysql -u root -p < /tmp/install.sql
```

Purge all caches 

## Usages

```
make install
make up
make down
make npm-install
make composer-install
make local-install
make directory-install
make persmission-install
make clear-cache
```

#### Prod example

```
make install DOCKER_COMPOSE_FILE=docker-compose.prod.yml SERVICE_APP_FILE=local.xml.prod
```

nota : local.xml.prod no exists

## Access

| Service        | Url                     | Login        | Password      |
| -------------- | ----------------------- | ------------ | ------------- |
| Front          | https://localhost/      | na           | na            |
| Back           | https://localhost/admin | admin        | admin123      |
| PhpMyAdmin     | http://localhost:8081/  | root         | password      |
| MailHog        | http://localhost:8025/  | na           | na            |
