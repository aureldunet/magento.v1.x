FROM debian:9 AS magento-v1x-apache

RUN apt-get update \
&&  apt-get install -y --no-install-recommends \
	software-properties-common \
	apt-transport-https \
	lsb-release \
	ca-certificates \
&&  apt-get update \
&&  apt-get install -y --no-install-recommends \
    gnupg \
    gnupg1 \
    gnupg2 \
	ssl-cert \
	git \
	vim \
	wget \
	curl \
	cron \
	unzip

RUN apt-get update \
&&  apt-get install -y --no-install-recommends \
	supervisor

COPY docker/supervisor/supervisord.conf /etc/supervisor/supervisord.conf
COPY docker/supervisor/conf.d/* /etc/supervisor/conf.d/

RUN apt-get update \
&&  wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - \
&&  echo "deb https://packages.sury.org/php/ stretch main" | tee /etc/apt/sources.list.d/php.list

RUN apt-get update \
&&  apt-get install -y \
    apache2 \
    libapache2-mod-php7.2 \
    php7.2 \
    php7.2-cli \
    php7.2-common \
    php7.2-curl \
    php7.2-soap \
    php7.2-iconv \
    php7.2-mbstring \
    php7.2-mysql \
    php7.2-xml \
    php7.2-gd \
    php7.2-intl \
    php7.2-zip

COPY docker/php/* /etc/php/7.3/cli/

RUN rm -rf /var/www/html/*

COPY docker/apache/sites-available/*.conf /etc/apache2/sites-available/

COPY docker/apache/*.conf /etc/apache2/

RUN rm /etc/apache2/sites-enabled/*

RUN ln -sf /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-enabled/000-default.conf

RUN ln -sf /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-enabled/default-ssl.conf

RUN a2enmod rewrite \
&&  a2enmod headers \
&&  a2enmod ssl

RUN chown -Rf www-data:www-data /var/www /var/www/html/.* \
&&  usermod -u 1000 www-data \
&&  chsh -s /bin/bash www-data

ENV LANG               C
ENV APACHE_RUN_USER    www-data
ENV APACHE_RUN_GROUP   www-data
ENV APACHE_PID_FILE    /var/run/apache2.pid
ENV APACHE_RUN_DIR     /var/run/apache2
ENV APACHE_LOCK_DIR    /var/lock/apache2
ENV APACHE_LOG_DIR     /var/log/apache2

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer

WORKDIR /var/www/html

CMD ["/usr/bin/supervisord"]

FROM debian:9 AS magento-v1x-node

RUN apt-get update \
&&  apt-get install -y --no-install-recommends \
	software-properties-common \
	apt-transport-https \
	lsb-release \
	ca-certificates \
&&  apt-get update \
&&  apt-get install -y --no-install-recommends \
    gnupg \
    gnupg1 \
    gnupg2 \
	git \
	vim \
	nano \
	wget \
	curl \
	unzip

RUN apt-get update \
&&  curl -sL https://deb.nodesource.com/setup_10.x | bash - \
&&  apt-get install -y --no-install-recommends \
    nodejs

RUN npm install gulp -g

RUN mkdir -p /var/www/html \
&&  chown -Rf www-data:www-data /var/www /var/www/html/.* \
&&  usermod -u 1000 www-data \
&&  chsh -s /bin/bash www-data

WORKDIR /var/www/html
