# Makefile

APP_ENV					?= dev
APP_UID					?= 1000
APP_GROUP				?= www-data
APP_COMPOSE				?= docker-compose.yml
APP_CONFIG				?= local.xml.dist

include .env

DOCKER_COMPOSE 			= docker-compose
DOCKER_COMPOSE_EXEC 	= $(DOCKER_COMPOSE) exec
DOCKER_COMPOSE_RUN 		= $(DOCKER_COMPOSE) run

EXEC_APACHE 			= $(DOCKER_COMPOSE_EXEC) apache
EXEC_APACHE_USER 		= $(DOCKER_COMPOSE_EXEC) -u $(APP_GROUP) apache
EXEC_APACHE_COMPOSER 	= $(EXEC_APACHE_USER) composer

RUN_NODE				= $(DOCKER_COMPOSE_RUN) node
RUN_NODE_USER			= $(DOCKER_COMPOSE_RUN) -u $(APP_GROUP) node

up:
	$(DOCKER_COMPOSE) -f $(APP_COMPOSE) up -d --build

down:
	$(DOCKER_COMPOSE) down -v

directory-install:
	$(EXEC_APACHE_USER) mkdir -p ./var
	$(EXEC_APACHE_USER) mkdir -p ./media

local-install:
	$(EXEC_APACHE_USER) cp ./app/etc/$(APP_CONFIG) ./app/etc/local.xml

composer-install:
	$(EXEC_APACHE_COMPOSER) install --no-suggest --no-progress

npm-install:
	$(RUN_NODE_USER) npm install

install: up directory-install local-install composer-install npm-install
